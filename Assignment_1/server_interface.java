
import java.rmi.*;

interface ServerInterface extends Remote{
    
    public String concatStrings(String str1, String str2) throws RemoteException;
    public String reverseString(String str1) throws RemoteException;
    public boolean equalStrings(String str1, String str2) throws RemoteException;
    public char charAt(String str1, int index) throws RemoteException;

}