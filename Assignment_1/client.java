import java.io.*;
import java.rmi.*;

class Client {
    public static void main(String[] args) throws IOException, NotBoundException {

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        String serverURL = "rmi://localhost/Server";
        ServerInterface sobj = (ServerInterface) Naming.lookup(serverURL);

        System.out.println("*** Welcome ***");
        boolean flag = true;

        while (flag) {

            System.out.println("\n*** Operations on String ***");
            System.out.println("1. Concat String");
            System.out.println("2. Reverse String");
            System.out.println("3. Equal Strings?");
            System.out.println("4. Character At Index");
            System.out.println("5. Exit");

            System.out.print("\nEnter your choice :: ");
            int choice = Integer.parseInt(br.readLine());

            switch (choice) {
                case 1: {
                    System.out.print("\nEnter String1 :: ");
                    String str1 = br.readLine();

                    System.out.print("Enter String2 :: ");
                    String str2 = br.readLine();

                    System.out.println("\nConcatenated String :: " + sobj.concatStrings(str1, str2));
                }
                    break;

                case 2: {
                    System.out.print("\nEnter String :: ");
                    String str1 = br.readLine();

                    System.out.println("\nReversed String :: " + sobj.reverseString(str1));
                }
                    break;

                case 3: {
                    System.out.print("\nEnter String1 :: ");
                    String str1 = br.readLine();

                    System.out.print("Enter String2 :: ");
                    String str2 = br.readLine();

                    if (sobj.equalStrings(str1, str2))
                        System.out.println("\nStrings are equal");
                    else
                        System.out.println("\nStrings are not equal");
                }
                    break;

                case 4: {
                    System.out.print("\nEnter String :: ");
                    String str1 = br.readLine();

                    System.out.print("Enter index :: ");
                    int index = Integer.parseInt(br.readLine());
                    
                    System.out.println("\nCharacter at index :: " + sobj.charAt(str1, index));

                }
                    break;

                case 5:
                    flag = false;
                    break;

                default:
                    System.out.println("\nPlease Enter valid choice");
                    break;
            }
        }

        br.close();
    }
}