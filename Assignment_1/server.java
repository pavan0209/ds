import java.rmi.*;

class Server {

    public static void main(String[] args) {
        
        try {
            ServerImplementation sobj = new ServerImplementation();
            Naming.rebind("Server", sobj);

            System.out.println("Server Started..");

        } catch (Exception e) {
            System.out.println("Exception occurred at server : " + e.getMessage());
        }
    }
}