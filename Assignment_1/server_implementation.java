
import java.rmi.*;
import java.rmi.server.*;

 class ServerImplementation extends UnicastRemoteObject implements ServerInterface{

    ServerImplementation() throws RemoteException {
        
    }

    public String concatStrings(String str1, String str2) throws RemoteException {
        return str1 + str2;
    }

    public String reverseString(String str1) throws RemoteException {
        return new StringBuffer(str1).reverse().toString();
    }

    public boolean equalStrings(String str1, String str2) throws RemoteException {
        return str1.equals(str2);
    }

    public char charAt(String str1, int index) throws RemoteException {
        if(index < 0 || index >= str1.length()) {

            System.out.println("Index out of range");
            return (char)(-1);
        } 
        return str1.charAt(index);
    }
}