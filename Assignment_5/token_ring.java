import java.util.*;
class TokenRing{

    public static void main(String[] args) {
        
        Scanner sc = new Scanner(System.in);

        System.out.print("\nEnter number of nodes you want in the ring :: ");
        int n = sc.nextInt();
        System.out.println("\nRing formed is as below : ");

        for(int i = 0; i < n; i++) {
            System.out.print(i + " ");
        }

        System.out.println("0");
        int choice = 0;

        do {

            System.out.print("\nEnter Sender :: ");
            int sender = sc.nextInt();

            System.out.print("Enter Receiver :: ");
            int receiver = sc.nextInt();
            
            System.out.print("\nEnter data to be send :: ");
            int data = sc.nextInt();

            int token = 0;
            System.out.print("\nToken Passing :: ");

            for(int i = token; i < sender; i++) {
                System.out.print(i + " -> ");
            }

            System.out.println(sender);
            System.out.println("\nSender : " + sender + " is sending data : " + data + " to receiver : " + receiver + "\n");

            for(int i = sender; i != receiver; i = (i+1)%n) {
                System.out.println("Data : " + data + " forwarded by : " + i);
            }

            System.out.println("\nReciever : " + receiver + " received data : " + data);
            token = sender;

            System.out.print("\nDo you want to send data again?? (If YES enter 1, If NO enter 0) :: ");
            choice = sc.nextInt();

        }while(choice == 1);
    }
}